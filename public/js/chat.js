const raidVotingContainer = document.querySelector('#raidVotingContainer');
const raidTargetListContainer = document.querySelector('#raidSuggestionListContainer');
const raidTargetListElement = document.querySelector('#raidSuggestionList');
const voteSlots = Array.from(document.querySelectorAll('.raidTargetVoteDisplay'));
const raidWinnerElement = document.querySelector('#raidVoteWinnerContainer');
const suggestionTimerElement = document.querySelector('#suggestionTimer');
const suggestionMinutesElement = suggestionTimerElement.querySelector('.mins');
const suggestionSecondsElement = suggestionTimerElement.querySelector('.secs');
let countdown;
// let timerSeconds;

console.log(voteSlots);
console.log(raidTargetListElement);

function printRaidTarget(value, key, map){
    console.log(`FROM CHATJS: ${key} of ${map.size}: ${value}`)
};

function createListFromRaidTargets(value, key, map){
     return `
        <li>
        ${key} = id="item${i}" ${plate.done ? 'checked' : ''} />
        <label for="item${i}">${plate.text}</label>
        </li>
    `;
};

function populateList(suggestions = [], raidList) {
    raidList.innerHTML = suggestions.map((target, i) => {
        const [name, obj] =  target; 
        // console.log(`${name}: (${obj.category}) - ${obj.viewers} viewers`);
        
      return `
        <li>
            ${name}: (${obj.category}) - ${obj.viewers} viewers
        </li>
      `;
    }).join('');  
}


function setupVoteOption({streamerInfo, votes,}, panelElement){
    panelElement.innerHTML = `<p class="name">${streamerInfo.displayName}</p>
    <img src="${streamerInfo.streamThumbnail}">
    <p class="category">(${streamerInfo.category})</p>
    <p class="title">${streamerInfo.streamTitle}</p>
    <p class="viewers">Viewers: ${streamerInfo.viewers}</p>
    <p class="followers">Followers: ${streamerInfo.followers}</p>
    <div class="voteAmountDisplay">
        <p>${votes}</p>
    </div>   
    `
}

function updateVote(panelElement, votes, streamerInfo){
    panelElement.innerHTML = `<p class="name">${streamerInfo.displayName}</p>
    <img src="${streamerInfo.streamThumbnail}">
    <p class="category">(${streamerInfo.category})</p>
    <p class="title">${streamerInfo.streamTitle}</p>
    <p class="viewers">Viewers: ${streamerInfo.viewers}</p>
    <p class="followers">Followers: ${streamerInfo.followers}</p>
    <div class="voteAmountDisplay">
        <p>${votes}</p>
    </div>   
    `
}

function InitTimer(minsEl, secsEl, durationInMins){
    
        clearInterval(countdown);
        let seconds = durationInMins * 60;
        const now = Date.now();
        const then = now + seconds * 1000;
        displayTimeLeft(seconds);
      
        countdown = setInterval(() => {
          const secondsLeft = Math.round((then - Date.now()) / 1000);
          // check if we should stop it!
          if(secondsLeft < 0) {
            //SOCKET EVENT!?
            clearInterval(countdown);
            return;
          }
          // display it
          displayTimeLeft(secondsLeft);
        }, 1000);
      
        function displayTimeLeft(seconds){

            let mins = Math.floor(seconds / (60));
    
            let secs = Math.ceil((seconds % (60)));
        
            mins = secs >= 60 ? mins + 1 : mins;
            secs = secs >= 60 ? 0 : secs;
        
            // mins = secs > 59000 ? mins + 1000 : secs;
            minsEl.innerText = String(mins).padStart(2,'0');
            secsEl.innerText = String(secs).padStart(2,'0'); //.substring(0, 2)
        }

}


//short hand for "when the dom is ready"
document.addEventListener("DOMContentLoaded", function() {

    //make connection
 var socket = io.connect('http://localhost:3000')

 //buttons and inputs
 const infoMessageElement = document.getElementById('info-message');
 const testButton = document.getElementById('test-button');


 //Emit message
 testButton.addEventListener('click', function(){
     socket.emit('test_event', {test_data : "Honest Dan is aight"})
 });

 //Listen on new_message
 socket.on("test_event", (data) => {
     infoMessageElement.innerHTML = `The data is <b> ${data.test_data}.`;
 });
 
//  socket.on('reload', () => window.location.reload());

 //End of raid vote command
 socket.on('raidSuggestionsFinished', (data) => {
     populateList(data, raidTargetListElement);
    //  console.log((new Map(data)));
    //  raidTargetListElement.innerHTML = (new Map(data)).forEach(printRaidTarget).join('');
 });

 socket.on('onValidRaidSuggestion', (data) => {
    populateList(data, raidTargetListElement);
 });

 socket.on('onVotingStarted', (data) =>{
    //go through each one
    //fill the details
    raidTargetListContainer.classList.add('hidden');
    raidVotingContainer.classList.remove('hidden');
    console.log("In onVotingStarted socket event");
    console.log(data);
    for (let i = 0; i < data.length; i++) {
        
        console.log("Iteration ", i);
        console.log(data[i][1]);
        setupVoteOption(data[i][1], voteSlots[i]);
        voteSlots[i].classList.remove('hidden');
     }
     for (let i = data.length; i < voteSlots.length; i++){
         //if we had 2 passed in but there are 3 slots to show
         //go from slot 3 up to disable them
         //0, 1 taken, need 2 until 2
         voteSlots[i].classList.add('hidden');
     }
 });

 socket.on('onVoteReceived', ({voteChoice, votes, streamerInfo,}) => {
    const panel = document.querySelector(`[data-voteslot=${voteChoice}]`)
    updateVote(panel, votes, streamerInfo);
 });

 socket.on('onVotingEnded', ({streamerInfo, votes}) => {
    raidVotingContainer.classList.add('hidden');
    raidWinnerElement.classList.remove('hidden');
    raidWinnerElement.innerHTML = `<h2>AND THE WINNER IS...</h2>
    <h1>${streamerInfo.displayName}</h1>
    <h2>${streamerInfo.streamTitle} (${streamerInfo.category}) with ${votes} votes.</h2>
    <img src="${streamerInfo.streamThumbnail}">
    <p>Please join us on the !raid by copying and pasting the raid message and pasting it once in the raid targets chat once we have been moved automatically to their chat.</p>
`;
    setTimeout(() => raidWinnerElement.classList.add('hidden'), 20000);
 });

 socket.on('onSuggestionStart', ({duration}) => {
     console.log("IM IN THE CLIENT AND ON SUGGESTION START");
     raidTargetListContainer.classList.remove('hidden');
     if(duration){
         suggestionTimerElement.classList.remove('hidden');
         InitTimer(suggestionMinutesElement, suggestionSecondsElement, duration);
     }else{
        suggestionTimerElement.classList.add('hidden');
     }
 });
 
});




