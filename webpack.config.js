const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

exports.default = {
    mode: 'development',
    context: path.resolve(__dirname, 'app'),
    entry: {
        index: './index.js',
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: './js/[name].bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
};
