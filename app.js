require('dotenv').config();
const TwitchJs = require('twitch-js');
const express = require('express');
const app = express();
let config = require('./data/config.json');
// const scope = { commandMap, config, }
const { isValidCommand, processCommand } = require( './src/commands' );
// let hasConnectedFirstTime;

//this mounts the folder public to the / path on the server, making stuff accesdsible via an endpoint
app.use(express.static('public'));

//routing (request and response)
app.get('/', (req, res) => {
    //if someone makes a request to the / endpoint, respond with the string gentle message
    res.render('index.html');   //render means serve html
});


server = app.listen(3000);
//socket init
const io = require("socket.io")(server);

//listen on every connection
io.on('connection', (socket) => {
    console.count("New user connected");

    socket.on('test_event', (data) => {
        console.log(`Received event from client ${data.test_data}`);
    });

    //listen on new message
    socket.on('test_event', (data) => {
        //broadcasts the new message
        // .sockets represents all sockets connected
        io.sockets.emit('test_event', { test_data: `Back from server: ${data.test_data}` });
    });

    // if(!hasConnectedFirstTime){
    //     io.sockets.emit('reload');
    //     hasConnectedFirstTime = true;
    // }
   
});



const client = new TwitchJs.client({
    options: {
        debug: true
    },
    connection: {
        reconnect: true,
    },
    //irc channels start with #
    channels: [`#${process.env.TWITCH_CHANNEL}`],
    identity: {
        username: process.env.TWITCH_BOT_NAME,
        password: process.env.TWITCH_OAUTH
    },
});

//setting up events for the twitch client to listen for
client.on('chat', onMessage);
client.on('connected', (address, port) => console.log(`Connected: ${address}:${port}`));
client.on('reconnect', () => console.log('Reconnecting'));
client.connect();

//userstate is an object with properties we'll access
async function onMessage(channel, userstate, message, self) {
   
    if (self) return;

    const messageMetaData = {
        isStreamer: userstate.username === process.env.TWITCH_CHANNEL,
        isMod: userstate.mod,
        isSub: userstate.subscriber,
        badges: userstate.badges,
        username: userstate.username,
        userId: userstate['user-id']
    };

    if (isValidCommand(message, config)) {

        try{
            const {messageToPrint, otherToDo, socketEvents} = await processCommand(message, messageMetaData, client);
             // { wasCommandSuccessful, printToChat, messageToPrint };
            if(messageToPrint){
                client.say(process.env.TWITCH_CHANNEL, messageToPrint);
            }
            if(otherToDo){
                otherToDo(client);
            }
            if (socketEvents) {
                //if it exists, it is an array of events to emit
                //using deconstruction of the passed in event-objects above
                //io is in our scope here, we can just ask it to emit to all clients
                socketEvents.forEach(({name, data})=> io.sockets.emit(name, data));
            }
        }catch(e){
            console.warn("An error was caught: "+e);
        }

    }

    if (channel === `#${process.env.TWITCH_CHANNEL.toLowerCase()}` && message.toLowerCase() === "hail dan") {
        //we access properties using bracket notation if they include a dash
        client.say(process.env.TWITCH_CHANNEL, `${userstate["display-name"]} is very wise...`);
    }

}

