    const fs = require('fs');
    const path = require('path');

    function saveJsonToFile(object, fileName) {
        
        //write quick and dirty. stringify with indentation of 4 for prettyness
        fs.writeFileSync(path.resolve(__dirname, `../data/${fileName}.json`), JSON.stringify(object, null, 4));
    }

    function doesStringContain(haystack, needle){
        
    }

    function alphanumeric(inputtxt)
    { 
        var letters = /^[0-9a-zA-Z]+$/;
        return inputtxt.match(letters);       
    }

    module.exports = { saveJsonToFile, alphanumeric};
