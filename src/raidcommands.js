let excludedRaidTargets = require('../data/excludedRaidTargets.json');
const { saveJsonToFile } = require('./util');
// let textCommands = require('../data/commands.json');
const { alphanumeric } = require('./util');
let config = require('../data/config.json');
const raidTargetMap = new Map();
let suggestionsActive = false;

let votingActive = false;
const votingMap = new Map();
const votingLabels = ['a', 'b', 'c','d','e'];
let voters = [];

let suggestionTimeout;

updateExcludedRaidTargets();

async function startRaidSuggestions(args) {
    let data = {};
    if(args.length > 0){
        let timerMins = args[0];
        timerMins = Number.parseInt(timerMins);
        if(!Number.isNaN(timerMins)){
            //min / max amount in here
            data = { duration: timerMins }
            // suggestionTimeout = setTimeout(stopRaidSuggestions, timerMins * 60 * 1000);
        }
    }

    raidTargetMap.clear();
    suggestionsActive = true;
    
    return {
        messageToPrint: 'Please start providing your raid suggestions in chat. Use the command "+s" followed by the URL of the streamer',
        otherToDo: () => console.log("I am doing my otherToDo - startRaidSuggestion"),
        socketEvents: [{name: 'onSuggestionStart', data}]
    }
}

async function stopRaidSuggestions() {
    if(suggestionTimeout){
        clearTimeout(suggestionTimeout);
    }

    suggestionsActive = false;
    var data = Array.from(raidTargetMap.entries());
    console.log(data);
    return {
        messageToPrint: 'Raid suggestions are no longer open.',
        otherToDo: () => console.log("I am doing my otherToDo - stopRaidSuggestions"),
        
        socketEvents: [{name: "raidSuggestionsFinished", data,}]    //if we dont do data: data, it will name the key data automatically
    }
}
// const optionsString = votingCandidates.map((element, index) => { return `[${votingLabels[index]}] ${element}`; }).join("");
async function startRaidVoting() {
    const votingCandidates = getUpToThreeRaidTargets();   
    console.log(votingCandidates);
    votingMap.clear();
    voters = [];

    let optionsString = "OPTIONS:";
    votingCandidates.forEach((streamerInfo, index) => {
        let currentLabel = votingLabels[index];
        votingMap.set(currentLabel, { streamerInfo, votes: 0, });       
        optionsString += ` [${votingLabels[index]}] ${streamerInfo.displayName}`;
    });
    optionsString += ".";

    votingActive = true;
    var data = Array.from(votingMap.entries());
    return {
        messageToPrint: 'Please start voting for who you\'d like to raid. Use the "+vote" command followed by either option a, b or c which corresponds to the options you see on screen.',
        otherToDo: (client) => { 
            
            return client.say(process.env.TWITCH_CHANNEL, optionsString); 
        },
        socketEvents: [{name: "onVotingStarted", data,}]
    }
}

async function stopRaidVoting() {
    votingActive = false;
    // console.log(votingMap);
    //STUPID VOTINGMAP CAUSING ALL THE STUPID PROBLEMS 
    let highestVotedTargets = Array.from(votingMap).reduce((winners, [name,curr], index) => {
        //index 0, add it on (since 0 is a falsey)
        console.log("curr is ",curr);
        if (!index) {
          winners.push(curr);
        } else {
          if (curr.votes > winners[0].votes) {
            winners = [curr];
            //if equal, we want to keep both
          } else if (curr.votes === winners[0].votes) {
            winners.push(curr);
          }
        }
        return winners;
      }, []);

      console.table(highestVotedTargets);

    let data; //must use let here, as we aren't setting it at the same time as defining it. also defining it within the if statement would restrict its scope to be within the if statement.
    if(highestVotedTargets.length > 1){
        //MORE THAN ONE WINNER - DO SOMETHING
        //TODO: winner = chooseBetweenTie(highestVotedTargets);
        data = highestVotedTargets[Math.round(Math.random() * highestVotedTargets.length-1)];
       
    }else{
        data = highestVotedTargets[0];
    }


    // {"a", { streamerInfo: { xxx: x, yyy: y}, votes: 4}}
    // {"b", { streamerInfo: { xxx: x, yyy: y}, votes: 2}}
    // {"c", { streamerInfo: { xxx: x, yyy: y}, votes: 6}}
    console.log(data);
 
    excludedRaidTargets[data.streamerInfo.displayName] = new Date();
    saveJsonToFile(excludedRaidTargets, 'excludedRaidTargets');

    return {
        messageToPrint: `Voting has now closed for our raid target. The winner is... ${data.streamerInfo.displayName}! LETS GO! (Dan, copy this: /raid ${data.streamerInfo.displayName})`,
        // otherToDo: () => console.log(votingMap),
        socketEvents: [{name: "onVotingEnded", data: data,}]
    }
}

async function showRaidTargets() {
    let raiderTargetString = "";
    raidTargetMap.forEach(function logMapElements(value, key, map) {
        raiderTargetString += ` ${key} (${value.category})`
      });
    return {
        messageToPrint: raiderTargetString,
    }
}

async function suggest(args, metaData, client) {
    if(!suggestionsActive){
        return { messageToPrint: 'Raid suggestions are not currently active.', };
    }
    
    //+suggest https://www.twitch.tv/honestdangames
    if (args.length <= 0) {
        return { messageToPrint: 'suggest needs to be followed by the URL of the streamer', };
    }

    const urlSuggestion = args[0];
    let targetName = validChannelUrl(urlSuggestion);
    if (targetName.length <= 0) {
        targetName = validUsername(urlSuggestion);
        //this point, we either have '' or the name of the suggested streamer
        if (targetName.length <= 0) {
            return { messageToPrint: 'suggestion error: you did not provide a valid channel url or username', };
        }
    }
    
    try {
        const targetChannelId = await getIdWithUsername(targetName, client);
        const streamData = await validRaidTarget(targetChannelId, client);
        if (streamData.stream) {
            // return "YAY";
            // console.log(streamData);
            const raidTargetInfo = {
                displayName: streamData.stream.channel.display_name,
                streamThumbnail: streamData.stream.preview.large,
                category: streamData.stream.game,
                streamTitle: streamData.stream.channel.status,
                viewers: streamData.stream.viewers,
                followers: streamData.stream.channel.followers,
                recommendedBy: metaData.username
            }

            if(excludedRaidTargets.hasOwnProperty(targetName)){
                return { messageToPrint: `Raid target ${raidTargetInfo.displayName} can't be suggested as they 
                were raided with the last ${config.raidTargetCooldownInDays} days (${new Date(excludedRaidTargets[targetName]).toLocaleDateString()}).`}
            }

            if (tryToAddToRaidTargetMap(raidTargetInfo.displayName, raidTargetInfo)) {
                var data = Array.from(raidTargetMap.entries());
                return { 
                    messageToPrint: `Raid target: ${streamData.stream.channel.display_name} with ${streamData.stream.viewers} viewers`,
                    socketEvents: [{name: "onValidRaidSuggestion", data,}]}
            } else {
                return { messageToPrint: `Raid target ${streamData.stream.channel.display_name} has already been suggested.`, };
            }

        } else {
            return { messageToPrint: 'invalid raid target (not valid user or live)', };
        }
    } catch (e) {
        return { messageToPrint: `error encountered: ${e}`, };
    }
}

async function vote(args, metaData){
    if(!votingActive){
        return { messageToPrint: 'Raid voting is not currently active.', };
    }

    //+suggest https://www.twitch.tv/honestdangames
    if (args.length <= 0) {
        return { messageToPrint: `The vote command needs to be followed by a vote option (${votingLabels.join('')}).`, };
    }

    const voteChoice = args[0];
    if(!votingMap.has(voteChoice)){
        return { messageToPrint: `You didn't provide a valid vote option (${votingLabels.join('')}).`, };
    }else{
        if(voters.includes(metaData.userId)){
            return { messageToPrint: `${metaData.username} - you have already cast your vote.`}
        }
        voters.push(metaData.userId);
        const vote = votingMap.get(voteChoice);
        vote.votes += 1; 
        votingMap.set(voteChoice, vote);
        console.log(`Vote counted for ${voteChoice} : ${votingMap.get(voteChoice).streamerName}`);
        const data = { voteChoice, votes: votingMap.get(voteChoice).votes, streamerInfo: votingMap.get(voteChoice).streamerInfo};
        return {socketEvents: [{name: "onVoteReceived", data,}]};
    }
}

function tryToAddToRaidTargetMap(targetName, targetInfo) {
    if (!raidTargetMap.has(targetName.toLowerCase())) {
        console.log("Adding entry for "+targetName);
        raidTargetMap.set(targetName.toLowerCase(), targetInfo);
        return true;
    }
    console.log("Entry already exists for "+targetName);
    return false
}

function getIdWithUsername(username, client) {
    return new Promise((resolve, reject) => {
        client.api(
            {
                url: `https://api.twitch.tv/kraken/users?login=${username}`,
                headers: {
                    'Client-ID': process.env.TWITCH_CLIENT_ID,
                },
            },
            //callback function
            function (err, res, body) {
                if (err || !body) {
                    return reject(err);
                }
                if(body._total <= 0){
                    return reject("no user found by that username");
                }
                console.log(body);
                const firstUserId = body.users[0]._id;
                console.log(`${firstUserId} (${body.users[0].display_name})`);
                return resolve(firstUserId);
            },
        );
    });
}

function validChannelUrl(url) {
    url = url.toLowerCase();
    const finalSlash = url.lastIndexOf('/');
    //-1 if / wasn't found
    if (finalSlash < 0) {
        return '';
    }

    //nothing followed the final slash
    if (finalSlash + 1 == url.length) {
        return '';
        //https://www.twitch.tv/
    }

    if (!url.includes('twitch.tv')) {
        return '';
    }

    const targetName = url.substring(finalSlash + 1);

    return targetName;
}

function validUsername(username) {
    username = username.toLowerCase();
    if(!alphanumeric(username)){
        return '';
    }
    // if(username.match(/\s/g)){
    //     return '';
    // }
    return username;
}

//merl 153916767
//+suggest https://www.twitch.tv/honestdangames
async function validRaidTarget(userId, client) {
    return new Promise(async (resolve, reject) => {

        let streamData;

        try {
            streamData = await getTwitchUserData(userId, client);
        } catch (e) {
            return reject(e);
        }

        // do more using userData
        const isTargetLive = streamData.stream !== null;
        if (!isTargetLive) {
            return reject("target isnt' live");
        }
        resolve(streamData);
    });
}

async function getTwitchUserData(userId, client) {
    return new Promise((resolve, reject) => {
        client.api(
            {
                url: `https://api.twitch.tv/kraken/streams/${userId}`,
                headers: {
                    'Client-ID': process.env.TWITCH_CLIENT_ID,
                },
            },
            //callback function
            function (err, res, body) {
                if (err || !body) {
                    return reject(err);
                }

                return resolve(body);
            },
        );
    });
}

function getUpToThreeRaidTargets(){
    let amountToReturn = 3 <= raidTargetMap.size ? 3 : raidTargetMap.size;  //math.max(3, raidTargetMap.size);
    const allCandidates = Array.from(raidTargetMap.values());
    const selectedCandidates = [];
    for(let i=0; i<amountToReturn; i++){
        const randomIndex = Math.floor(Math.random()*allCandidates.length);
        let selectedCandidate = allCandidates[randomIndex];
        selectedCandidates.push(selectedCandidate);
        allCandidates.splice(randomIndex, 1);
    }
    return selectedCandidates;
}

function updateExcludedRaidTargets(){

    const now = new Date();
    const expireDate = now.setDate(now.getDate() - 7);
    console.log(expireDate.toLocaleString());

    const startAmount = Object.keys(excludedRaidTargets).length;
    Object.keys(excludedRaidTargets).forEach(targetName => {
        let raidDate = new Date(excludedRaidTargets[targetName]);
        // console.log(raidDate);
        if (raidDate < expireDate) {
            console.log(`Removing ${targetName} from excluded raid targets`);
            delete excludedRaidTargets[targetName];
        } else {
            
            console.log(`Exlcuded Raid Target: ${targetName} - until ${new Date(raidDate.setDate(raidDate.getDate() + 7)).toLocaleDateString()}`);
        }
    });

    if(startAmount > Object.keys(excludedRaidTargets).length){
        console.log("Saving excluded raid targets to file as there were changese.");
        saveJsonToFile(excludedRaidTargets, 'excludedRaidTargets');   
    }

}

function addToExcludeList(args){
    if(args.length <= 0){
        return { messageToPrint: `No name was provided to remove from the exclude list.`};
    }
    name = args[0].toLowerCase();
    excludedRaidTargets[name] = "manually excluded";
    saveJsonToFile(excludedRaidTargets, 'excludedRaidTargets');   
    return { messageToPrint: `${name} - has now been added to the exclude list.`}
}

function removeFromExcludeList(args){
    if(args.length <= 0){
        return { messageToPrint: `No name was provided to remove from the exclude list.`};
    }
    name = args[0].toLowerCase();
    if(excludedRaidTargets.hasOwnProperty(name)){
        delete excludedRaidTargets[name]; 
        saveJsonToFile(excludedRaidTargets, 'excludedRaidTargets');   
        return { messageToPrint: `${name} - is now free to be suggested as a raid target again. REJOICE.`};
    }else{
        return { messageToPrint: `${name} - is not in the exclude list.`}
    }
}

function removeSuggestion(args){
    if(raidTargetMap.count <= 0){
        return { messageToPrint: `There has been no suggestions so far.`, }
    }

    if(votingActive){
        return { messageToPrint: `Too late, voting is currently active.`, }
    }

    if(args.length <= 0){
        return { messageToPrint: `please follow the remove suggestion command with a streamer name`, }
    }
    
    const targetName = args[0].toLowerCase();
    if (!raidTargetMap.has(targetName)) {
        return  { messageToPrint: `${targetName} hasn't been suggested, silly.`, }
    }

    raidTargetMap.delete(targetName);   //returns true, could use it instead of the above...

    var data = Array.from(raidTargetMap.entries());
    return { 
        messageToPrint: `${targetName} has been removed from the suggestions.`,
        socketEvents: [{name: "onValidRaidSuggestion", data,}]}
}

function isValidTwitchUser() {

}

function isTwitchUserLive() {

}

module.exports = [
    ['start', { allow: ['streamer',], fn: startRaidSuggestions, },],
    ['stop', { allow: ['streamer',], fn: stopRaidSuggestions, }],
    ['startraidvoting', { allow: ['streamer',], fn: startRaidVoting, }],
    ['stopraidvoting', { allow: ['streamer',], fn: stopRaidVoting, }],
    ['s', { allow: ['all',], fn: suggest, }],
    ['showtargets', { allow: ['streamer',], fn: showRaidTargets, }],
    ['vote', { allow: ['all',], fn: vote, }],
    ['exclude', {allow: ['streamer'], fn: addToExcludeList, }] ,
    ['unexclude', {allow: ['streamer'], fn: removeFromExcludeList, }],
    ['remove', {allow: ['streamer'], fn: removeSuggestion, }],
]

//https://pastebin.com/YRGesvW9