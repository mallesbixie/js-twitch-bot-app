let textCommands = require('../data/commands.json');
const { saveJsonToFile } = require('./util');
const raidCommands = require('./raidcommands');

const commandMap = new Map([
    ['add', { allow: ['mods'], fn: addNewCommand, },],    
]);

//deconstruction of array, and assign to variables = "args will be an array, call the first thing key, and the second thing command"
raidCommands.forEach(([key, command]) => {
    if (!commandMap.has(key)) {
        commandMap.set(key, command);
    } else {
        console.log(`Loading command error: ${key} already exists in our commandMap`);
    }
});

// console.table(commandMap);

for (let key in textCommands) {
    if (textCommands.hasOwnProperty(key)) {
        if (!commandMap.has(key)) {
            commandMap.set(key, 
                { 
                    allow: ['all',], 
                    fn: async () => ({ 
                        messageToPrint: textCommands[key],
                    }), 
                }
            );
        } else {
            console.log(`Loading command error: ${key} already exists in our commandMap`);
        }
    }
}

function addNewCommand(args) {
    if (args.length < 2) {
        return 'To use the add command, please follow this format: +add <nameOfCommand> <textToShow>'
    }

    const newCommandName = args.shift().toLowerCase();
    const newCommandMessage = args.join(' ');

    if (!commandMap.has(newCommandName)) {
        commandMap.set(newCommandName, { allow: ['all'], fn: () => newCommandMessage, });
        textCommands[newCommandName] = newCommandMessage;
        saveJsonToFile(textCommands, 'commands');
        return `Adding new command: ${newCommandName}, will print out: ${newCommandMessage}.`;
    } else {
        return `The ${newCommandName} command already exists, please choose a unique command name`;
    }
}

async function processCommand(message, messageMetaData, client) {
    var args = message.trim().substring(1).split(' ');
    var command = args.shift().toLowerCase();
    // 
    // console.log("command: ", command, ", arguments: ", args);

    if (commandMap.has(command)) {
        const commandObj = commandMap.get(command);
        //check rights if not allowed for all
        if (!commandObj.allow.includes('all')) {
            //jump out with message if not allowed
            if (commandObj.allow.includes('mods') && !(messageMetaData.isStreamer || messageMetaData.isMod)) {
                // { wasCommandSuccessful, printToChat, messageToPrint };
                return {
                    messageToPrint: `Command ${command} only for mods you pleb!`,
                };
            }
            if (commandObj.allow.includes('streamer') && !messageMetaData.isStreamer) {
                return {
                    messageToPrint: `Command ${command} only for strimmer!`,
                }
                    
            }
        }
        //all fine, let the command return the message
        // console.log("returning command object "+commandObj.fn); 
        const result = await commandObj.fn(args, messageMetaData, client);   
        return result;

    } else {
        return `There exists no command: ${command}.`;
    }

}

function isValidCommand(message, config) {

    if (!startsWithCommandSymbol(message, config)) {
        return false;
    }
    if (!firstWordIsAnExistingCommand(message, config)) {
        return false;
    }
    return true;
}

function startsWithCommandSymbol(message, config) {
    if (message.startsWith(config.commandSymbol)) {
        return true;
    }
    return false;
}

function firstWordIsAnExistingCommand(message, config) {
    return true;
    //parse the first word
    //look through our json commands to see if word matches property name
}

module.exports = { processCommand, isValidCommand, };