import Vue from 'vue';

//include main component
import RaidSuggestions from './RaidSuggestions.vue';

//we might add components/plugins to Vue here later

//this magic line adds the component to the DOM.
//I also copy-paste it. It uses the createElement jsx code in the background
new Vue({
    render: h => h(RaidSuggestions),
}).$mount('#app');

